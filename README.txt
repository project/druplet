
Druplet.module - Conjur up Drupal sandboxes as subdomains using Multi-Site and available installation profiles

Druplet is an demand Drupal sandbox generating machine. It uses Drupal's multi-site
and install profile capabilities to allow for multiple unique sandboxes running under
the same codebase. This module is not meant for use on production servers. Rather,
it is a time-saving tool for developers. Druplet is currently in early development.
Feel free to download the code from CVS, but be warned that support is limited.

There are some critical server configuration issues regarding this module. Please read below for more detail.

== IMPORTANT! NEVER ALLOW UNTRUSTED USERS TO EXECUTE PHP CODE ON YOUR SERVER! ==
SECURITY WARNING: If you are using this for personal use, the security issues are
minimalized. However, if you allow non-trusted users access to execute PHP code on your
server, they can potentially read the settings.php file of another Druplet.
See http://groups.drupal.org/node/2989#comment-14133 for more info.

A few configuration items:
 - You must configure httpd.conf so that all subdomain traffic is directed to your primary DocumentRoot (i.e. druplet.mydomain.com -> mydomain.com)
 - The apache user must have write access to the sites directory. (chmod 777 sites)
 - The database user must be able to create databases and grant access. (GRANT USAGE ON * . * TO 'username'@'localhost' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0;)